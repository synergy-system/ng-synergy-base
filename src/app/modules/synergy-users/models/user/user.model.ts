export class UserModel {
  id?: string;
  email: string;
  salt: string;
  passwordHash: string;
  firstName: string;
  lastName: string;
  status: boolean;
  createdAt: Date;
  updatedAt: Date;
  version: number;
  roles?: string[];
}
