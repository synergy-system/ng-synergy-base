import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SynergyCoreModule } from '@synergy/core/synergy-core.module';
import { usersRoutes } from '@synergy/users/users.routing';
import { UsersAddComponent, UsersEditComponent, UsersListComponent, UsersUpsertComponent } from '@synergy/users/components';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { UsersService } from '@synergy/users/services/users/users.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule(
  {
    declarations: [UsersListComponent, UsersUpsertComponent, UsersAddComponent, UsersEditComponent],
    imports: [
      CommonModule,
      HttpClientModule,
      RouterModule.forChild(usersRoutes),
      ModalModule.forRoot(),
      SynergyCoreModule,
      FormsModule,
      ReactiveFormsModule
    ],
    exports: [],
    providers: [
      UsersService
    ]
  }
)
export class SynergyUsersModule {
}
