import { Routes } from '@angular/router';
import { UsersAddComponent, UsersEditComponent, UsersListComponent } from '@synergy/users/components';


export const usersRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      title: 'Gestion de Usuarios'
    },
    children: [
      {
        path: 'list',
        component: UsersListComponent,
        data: {
          title: 'Listado'
        }
      },
      {
        path: 'new',
        component: UsersAddComponent,
        data: {
          title: 'Registrar'
        }
      },
      {
        path: 'edit/:id',
        component: UsersEditComponent,
        data: {
          title: 'Editar'
        }
      }
    ]
  }
];
