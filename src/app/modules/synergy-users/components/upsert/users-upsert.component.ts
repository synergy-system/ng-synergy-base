import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputBaseConfigModel, transformationText } from '@synergy/core/form';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { ConfirmModalComponent } from '@synergy/core/modals/confirm/confirm-modal.component';
import { UsersService } from '@synergy/users/services/users/users.service';
import { userUpsertModeConfig } from '@synergy/users/config';
import { UserModel } from '@synergy/users/models/user/user.model';

@Component({
  selector: 'app-synergy-users-upsert',
  templateUrl: 'users-upsert.component.html',
})
export class UsersUpsertComponent implements OnInit, OnChanges {

  @Input() public mode: userUpsertModeConfig;
  @Input() recordId?: string;
  public form: FormGroup;
  public firstNameConfig: InputBaseConfigModel;
  public lastNameConfig: InputBaseConfigModel;
  public emailConfig: InputBaseConfigModel;
  public statusConfig: InputBaseConfigModel;
  public labelBtnSave: string;
  public confirmModal: BsModalRef;
  private requestSend = false;

  constructor( private formBuilder: FormBuilder,
               private usersService: UsersService,
               private readonly toastrService: ToastrService,
               private readonly router: Router,
               private modalService: BsModalService ) {
    this.labelBtnSave = 'Guardar';
  }

  ngOnInit() {
    this.initialization();
    this.formConstruction();
  }

  ngOnChanges( changes: SimpleChanges ): void {
    console.log(`changes => ${ JSON.stringify(changes) }`);
    if ( changes.recordId && changes.recordId.currentValue !== '' ) {
      this.findRecord();
    }

    if ( changes.mode && changes.mode.currentValue ) {
      if ( changes.mode.currentValue === userUpsertModeConfig.edit ) {
        this.labelBtnSave = 'Actualizar';
      }
    }
  }

  public initialization() {
    this.firstNameConfig = {
      label: 'Nombres',
      placeholder: 'Nombres del Usuario',
      valueDefault: '',
      transformation: [transformationText.upperCase]
    };

    this.lastNameConfig = {
      label: 'Apellidos',
      placeholder: 'Apellidos del Usuario',
      valueDefault: '',
      transformation: [transformationText.upperCase]
    };

    this.emailConfig = {
      label: 'Correo Electronico',
      placeholder: 'correo@dominio.com',
      valueDefault: '',
    };

    this.statusConfig = {
      label: 'Estatus',
      placeholder: '',
      valueDefault: false,
    };
  }

  public OnSave() {
    if ( this.form.valid ) {
      if ( !this.requestSend ) {
        this.requestSend = true;
        if ( this.mode === userUpsertModeConfig.create ) {
          this.createRecord();
        }
        if ( this.mode === userUpsertModeConfig.edit ) {
          this.updateRecord();
        }
      }
    } else {
      this.toastrService.error('Verifique los Datos ingresados', 'Validacion');
    }
  }

  public OnDelete() {
    if ( this.mode === userUpsertModeConfig.edit ) {
      this.confirmModal = this.modalService.show(ConfirmModalComponent, {
        class: 'modal-dark',
        keyboard: false,
        ignoreBackdropClick: true
      });
      this.confirmModal.content.title = 'Gestion de Usuarios';
      this.confirmModal.content.content = 'Favor confirmar que desea ELIMINAR el usuario seleccionado.';
      this.confirmModal.content.onClose.subscribe(ask => {
        if ( ask ) {
          this.deleteRecord();
        }
      });
    }
  }

  public OnReset() {
    console.log(`this.mode => ${ this.mode }`);
    if ( this.mode === userUpsertModeConfig.edit ) {
      this.findRecord();
    } else {
      this.initialization();
      this.form.reset();
    }
  }

  public OnFirstNameEmit( $event ) {
    this.form.controls.firstName.setValue($event);
  }

  public OnLastNameEmit( $event ) {
    this.form.controls.lastName.setValue($event);
  }

  public OnEmailEmit( $event ) {
    this.form.controls.email.setValue($event);
  }

  public OnStatusEmit( $event ) {
    console.log(`OnStatusEmit => ${$event}`);
    this.form.controls.status.setValue($event);
  }

  private formConstruction() {
    this.form = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.max(20)])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.max(20)])],
      status: ['', Validators.compose([Validators.required])],
    });
  }

  private preparedMessage(): Partial<UserModel> {
    if ( this.mode === userUpsertModeConfig.create ) {
      return {
        email: this.form.controls.email.value,
        firstName: this.form.controls.firstName.value,
        lastName: this.form.controls.lastName.value,
        status: this.form.controls.status.value,
      };
    } else {
      console.log(`this.form.controls.status.value => ${this.form.controls.status.value}`);
      return {
        id: this.recordId,
        email: this.form.controls.email.value,
        firstName: this.form.controls.firstName.value,
        lastName: this.form.controls.lastName.value,
        status: this.form.controls.status.value
      };
    }
  }

  private findRecord() {
    this.usersService.getRecord(this.recordId).subscribe(data => {
      this.firstNameConfig.valueDefault = data.firstName;
      this.firstNameConfig = Object.assign({}, this.firstNameConfig);
      this.lastNameConfig.valueDefault = data.lastName;
      this.lastNameConfig = Object.assign({}, this.lastNameConfig);
      this.emailConfig.valueDefault = data.email;
      this.emailConfig = Object.assign({}, this.emailConfig);
      this.statusConfig.valueDefault = data.status;
      this.statusConfig = Object.assign({}, this.statusConfig);
    });
  }

  private createRecord() {
    const record = this.preparedMessage();
    this.usersService.postCreate(record).subscribe(data => {
      this.toastrService.success('Registro exitoso', 'Registro de Rol');
      this.initialization();
      this.OnReset();
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Rol');
    });
  }

  private updateRecord() {
    const record = this.preparedMessage();
    this.usersService.putRecord(this.recordId, record).subscribe(data => {
      this.router.navigateByUrl('/admin/users/list').then(a => {
        this.toastrService.success('Registro Actualizado exitosamente', 'Registro de Usuarios');
      });
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Usuarios');
    });
  }

  private deleteRecord() {
    this.usersService.deleteRecord(this.recordId).subscribe(data => {
      this.router.navigateByUrl('/admin/users/list').then(a => {
        this.toastrService.success('Registro eliminado exitosamente', 'Registro de Rol');
      });
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Rol');
    });
  }

}
