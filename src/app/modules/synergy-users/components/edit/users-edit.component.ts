import { Component, OnInit } from '@angular/core';
import { userUpsertModeConfig } from '@synergy/users/config';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: 'users-edit.component.html',
  selector: 'app-synergy-users-add'
})
export class UsersEditComponent implements OnInit {
  upsertMode: userUpsertModeConfig;
  recordId: string;

  constructor( private route: ActivatedRoute ) {
    this.upsertMode = userUpsertModeConfig.edit;
  }

  ngOnInit(): void {
    this.recordId = this.route.snapshot.paramMap.get('id');
  }
}
