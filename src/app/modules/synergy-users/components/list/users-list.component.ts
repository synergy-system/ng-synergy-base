import { Component, OnInit } from '@angular/core';
import { Pagination } from '@synergy/core/models/pagination.model';
import { IPage } from '@synergy/core/interfaces/page.interface';
import { UsersService } from '@synergy/users/services/users/users.service';
import { Router } from '@angular/router';
import { UserModel } from '@synergy/users/models/user/user.model';

@Component({
  templateUrl: 'users-list.component.html',
  selector: 'app-synergy-users-list'
})
export class UsersListComponent implements OnInit {

  public response: Pagination<UserModel>;
  public pages: IPage[];

  constructor( private userService: UsersService, private router: Router ) {
    this.pages = [];
  }

  ngOnInit() {
    this.getPageByNumber(1);
  }

  public getPageByNumber( numberPage: number ) {
    this.userService.getPageByNumber(numberPage).subscribe(data => {
      this.response = data;
      this.setPages();
    });
  }

  public getPageByUri( uri: string ) {
    this.userService.getPageByUri(uri).subscribe(data => {
      this.response = data;
      this.setPages();
    });
  }

  public OnEdit( id: string ) {
    this.router.navigateByUrl(`/admin/users/edit/${ id }`);
  }

  private setPages() {
    this.pages = [];
    for ( let page = 0; page < this.response.pageCount; page++ ) {
      this.pages.push({
        numberPage: page + 1,
        enable: true,
      });
    }
  }

}
