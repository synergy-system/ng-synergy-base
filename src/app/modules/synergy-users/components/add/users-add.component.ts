import { Component } from '@angular/core';
import { userUpsertModeConfig } from '@synergy/users/config';


@Component( {
  templateUrl: 'users-add.component.html',
  selector: 'app-synergy-users-add'
} )
export class UsersAddComponent {
  upsertMode: userUpsertModeConfig;

  constructor() {
    this.upsertMode = userUpsertModeConfig.create;
  }
}
