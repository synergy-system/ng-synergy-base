import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pagination } from '@synergy/core/models/pagination.model';
import { UserModel } from '@synergy/users/models/user/user.model';

@Injectable()
export class UsersService {
  constructor( private readonly http: HttpClient ) {
  }

  public getPageByNumber( page: number = 1, limit: number = 10 ) {
    return this.http.get<Pagination<UserModel>>(`http://localhost:3000/api/v1/admin/users?page=${ page }&limit=${ limit }`);
  }

  public getPageByUri( uri: string ) {
    return this.http.get<Pagination<UserModel>>(`${ uri }`);
  }

  public getRecord( id: string ) {
    return this.http.get<UserModel>(`http://localhost:3000/api/v1/admin/users/${ id }`);
  }

  public postCreate( record: Partial<UserModel> ) {
    return this.http.post<UserModel>('http://localhost:3000/api/v1/admin/users', record);
  }

  public putRecord( id: string, record: Partial<UserModel> ) {
    return this.http.put<UserModel>(`http://localhost:3000/api/v1/admin/users/${ id }`, record);
  }

  public deleteRecord( id: string ) {
    return this.http.delete(`http://localhost:3000/api/v1/admin/users/${ id }`);
  }
}
