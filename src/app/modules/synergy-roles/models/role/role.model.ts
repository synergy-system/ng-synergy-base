export class RoleModel {
  id: string;
  role: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
  version: number;
  users?: string[];
}
