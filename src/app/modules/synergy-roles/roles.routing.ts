import { Routes } from '@angular/router';
import { RolesAddComponent, RolesEditComponent, RolesListComponent } from '@synergy/roles/components';

export const rolesRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  },
  {
    path: '',
    data: {
      title: 'Gestion de Roles'
    },
    children: [
      {
        path: 'list',
        component: RolesListComponent,
        data: {
          title: 'Listado'
        }
      },
      {
        path: 'new',
        component: RolesAddComponent,
        data: {
          title: 'Registrar'
        }
      },
      {
        path: 'edit/:id',
        component: RolesEditComponent,
        data: {
          title: 'Editar'
        }
      }
    ]
  }
];
