import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pagination } from '@synergy/core/models/pagination.model';
import { RoleModel } from '@synergy/roles/models';

@Injectable()
export class RolesService {
  constructor( private readonly http: HttpClient ) {
  }

  public getPageByNumber( page: number = 1, limit: number = 10 ) {
    return this.http.get<Pagination<RoleModel>>(`http://localhost:3000/api/v1/admin/roles?page=${ page }&limit=${ limit }`);
  }

  public getPageByUri( uri: string ) {
    return this.http.get<Pagination<RoleModel>>(`${ uri }`);
  }

  public getRecord( id: string ) {
    return this.http.get<RoleModel>(`http://localhost:3000/api/v1/admin/roles/${ id }`);
  }

  public postCreate( record: Partial<RoleModel> ) {
    return this.http.post<RoleModel>('http://localhost:3000/api/v1/admin/roles', record);
  }

  public putRecord( id: string, record: Partial<RoleModel> ) {
    return this.http.put<RoleModel>(`http://localhost:3000/api/v1/admin/roles/${ id }`, record);
  }

  public deleteRecord( id: string ) {
    return this.http.delete(`http://localhost:3000/api/v1/admin/roles/${ id }`);
  }
}
