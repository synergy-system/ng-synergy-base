export * from '@synergy/roles/components/roles-add/roles-add.component';
export * from '@synergy/roles/components/roles-list/roles-list.component';
export * from '@synergy/roles/components/roles-upsert/roles-upsert.component';
export * from '@synergy/roles/components/roles-edit/roles-edit.component';
