import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { roleUpsertModeConfig } from '@synergy/roles/config/role-upsert/role.upsert.mode.config';

@Component({
  selector: 'app-roles-edit',
  templateUrl: './roles-edit.component.html',
})
export class RolesEditComponent implements OnInit {
  roleUpsertMode: roleUpsertModeConfig;
  recordId: string;

  constructor( private route: ActivatedRoute ) {
    this.roleUpsertMode = roleUpsertModeConfig.edit;
  }

  ngOnInit(): void {
    this.recordId = this.route.snapshot.paramMap.get('id');
  }
}
