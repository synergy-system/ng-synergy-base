import { Component, OnInit } from '@angular/core';
import { RolesService } from '@synergy/roles/services';
import { RoleModel } from '@synergy/roles/models';
import { Pagination } from '@synergy/core/models/pagination.model';
import { Router } from '@angular/router';
import { IPage } from '@synergy/core/interfaces/page.interface';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
})
export class RolesListComponent implements OnInit {

  public response: Pagination<RoleModel>;
  public pages: IPage[];

  constructor( private rolesService: RolesService, private router: Router ) {
    this.pages = [];
  }

  ngOnInit() {
    this.getPageByNumber(1);
  }

  public getPageByNumber( numberPage: number ) {
    this.rolesService.getPageByNumber(numberPage).subscribe(data => {
      this.response = data;
      this.setPages();
    });
  }

  public getPageByUri( uri: string ) {
    this.rolesService.getPageByUri(uri).subscribe(data => {
      this.response = data;
      this.setPages();
    });
  }

  public OnEdit(id: string) {
    this.router.navigateByUrl(`/admin/roles/edit/${id}`);
  }

  private setPages() {
    this.pages = [];
    for ( let page = 0; page < this.response.pageCount; page++ ) {
      this.pages.push({
        numberPage: page + 1,
        enable: true,
      });
    }
  }


}
