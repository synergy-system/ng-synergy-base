import { Component } from '@angular/core';
import { roleUpsertModeConfig } from '@synergy/roles/config/role-upsert/role.upsert.mode.config';



@Component( {
  selector: 'app-roles-add',
  templateUrl: './roles-add.component.html',
} )
export class RolesAddComponent {
  roleUpsertMode: roleUpsertModeConfig;

  constructor() {
    this.roleUpsertMode = roleUpsertModeConfig.create;
  }
}
