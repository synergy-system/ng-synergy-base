import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputBaseConfigModel, transformationText } from '@synergy/core/form';
import { RolesService } from '@synergy/roles/services';
import { RoleModel } from '@synergy/roles/models';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ConfirmModalComponent } from '@synergy/core/modals/confirm/confirm-modal.component';
import { roleUpsertModeConfig } from '@synergy/roles/config';


@Component({
  selector: 'app-roles-upsert',
  templateUrl: './roles-upsert.component.html',
})
export class RolesUpsertComponent implements OnInit, OnChanges {

  @Input() public mode: roleUpsertModeConfig;
  @Input() recordId?: string;
  public form: FormGroup;
  public roleConfig: InputBaseConfigModel;
  public descriptionConfig: InputBaseConfigModel;
  public labelBtnSave: string;
  public confirmModal: BsModalRef;
  private requestSend = false;

  constructor( private formBuilder: FormBuilder,
               private rolesService: RolesService,
               private readonly toastrService: ToastrService,
               private readonly router: Router,
               private modalService: BsModalService ) {
    this.labelBtnSave = 'Guardar';
  }

  ngOnInit() {
    this.initialization();
    this.formConstruction();
  }

  ngOnChanges( changes: SimpleChanges ): void {
    console.log(`changes => ${ JSON.stringify(changes) }`);
    if ( changes.recordId && changes.recordId.currentValue !== '' ) {
      this.findRecord();
    }

    if ( changes.mode && changes.mode.currentValue ) {
      if ( changes.mode.currentValue === roleUpsertModeConfig.edit ) {
        this.labelBtnSave = 'Actualizar';
      }
    }
  }

  public initialization() {
    this.roleConfig = {
      label: 'Rol',
      placeholder: 'Nombre del Rol',
      valueDefault: '',
      transformation: [transformationText.upperCase, transformationText.trim]
    };

    this.descriptionConfig = {
      label: 'Descripcion',
      placeholder: 'Breve descripcion del rol',
      valueDefault: '',
      transformation: []
    };
  }

  public OnSave() {
    if ( this.form.valid ) {
      if ( !this.requestSend ) {
        this.requestSend = true;
        if ( this.mode === roleUpsertModeConfig.create ) {
          this.createRecord();
        }
        if ( this.mode === roleUpsertModeConfig.edit ) {
          this.updateRecord();
        }
      }
    } else {
      this.toastrService.error('Verifique los Datos ingresados', 'Validacion');
    }
  }

  public OnDelete() {
    if ( this.mode === roleUpsertModeConfig.edit ) {
      this.confirmModal = this.modalService.show(ConfirmModalComponent, {
        class: 'modal-dark',
        keyboard: false,
        ignoreBackdropClick: true
      });
      this.confirmModal.content.title = 'Gestion de Roles';
      this.confirmModal.content.content = 'Favor confirmar que desea ELIMINAR el rol seleccionado.';
      this.confirmModal.content.onClose.subscribe(ask => {
        if ( ask ) {
          this.deleteRecord();
        }
      });
    }
  }

  public OnReset() {
    console.log(`this.mode => ${ this.mode }`);
    if ( this.mode === roleUpsertModeConfig.edit ) {
      this.findRecord();
    } else {
      this.initialization();
      this.form.reset();
    }
  }

  public OnRoleEmit( $event ) {
    this.form.controls.role.setValue($event);
  }

  public OnDescriptionEmit( $event ) {
    this.form.controls.description.setValue($event);
  }

  private formConstruction() {
    this.form = this.formBuilder.group({
      role: ['', Validators.compose([Validators.required, Validators.minLength(2), Validators.max(20)])],
      description: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.max(100)])],
    });
  }

  private preparedMessage(): Partial<RoleModel> {
    if ( this.mode === roleUpsertModeConfig.create ) {
      return {
        role: this.form.controls.role.value,
        description: this.form.controls.description.value
      };
    } else {
      return {
        id: this.recordId,
        role: this.form.controls.role.value,
        description: this.form.controls.description.value
      };
    }
  }

  private findRecord() {
    this.rolesService.getRecord(this.recordId).subscribe(data => {
      this.roleConfig.valueDefault = data.role;
      this.roleConfig = Object.assign({}, this.roleConfig);
      this.descriptionConfig.valueDefault = data.description;
      this.descriptionConfig = Object.assign({}, this.descriptionConfig);
    });
  }

  private createRecord() {
    const record = this.preparedMessage();
    this.rolesService.postCreate(record).subscribe(data => {
      this.toastrService.success('Registro exitoso', 'Registro de Rol');
      this.initialization();
      this.OnReset();
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Rol');
    });
  }

  private updateRecord() {
    const record = this.preparedMessage();
    this.rolesService.putRecord(this.recordId, record).subscribe(data => {
      this.router.navigateByUrl('/admin/roles/list').then(a => {
        this.toastrService.success('Registro Actualizado exitosamente', 'Registro de Rol');
      });
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Rol');
    });
  }

  private deleteRecord() {
    this.rolesService.deleteRecord(this.recordId).subscribe(data => {
      this.router.navigateByUrl('/admin/roles/list').then(a => {
        this.toastrService.success('Registro eliminado exitosamente', 'Registro de Rol');
      });
      this.requestSend = false;
    }, err => {
      this.requestSend = false;
      this.toastrService.error(err.message, 'Registro de Rol');
    });
  }

}
