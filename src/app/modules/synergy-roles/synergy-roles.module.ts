import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RolesAddComponent, RolesEditComponent, RolesListComponent, RolesUpsertComponent } from '@synergy/roles/components';
import { RouterModule } from '@angular/router';
import { rolesRoutes } from './roles.routing';
import { HttpClientModule } from '@angular/common/http';
import { RolesService } from '@synergy/roles/services';
import { SynergyCoreModule } from '@synergy/core/synergy-core.module';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RolesListComponent, RolesAddComponent, RolesUpsertComponent, RolesEditComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(rolesRoutes),
    ModalModule.forRoot(),
    SynergyCoreModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    RolesService,
  ]
})
export class SynergyRolesModule {
}
