import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FormInputComponent, FormSwitchComponent } from '@synergy/core/form/components';
import { InputUppercaseDirective } from '@synergy/core/directive';
import { ConfirmModalComponent } from './modals/confirm/confirm-modal.component';


@NgModule(
  {
    declarations: [
      FormInputComponent,
      FormSwitchComponent,
      InputUppercaseDirective,
      ConfirmModalComponent
    ],
    exports: [
      FormInputComponent,
      FormSwitchComponent,
      InputUppercaseDirective
    ],
    providers: [
      InputUppercaseDirective
    ],
    imports: [
      CommonModule,
      FormsModule
    ],
    entryComponents: [
      ConfirmModalComponent
    ]
  }
)
export class SynergyCoreModule {
}
