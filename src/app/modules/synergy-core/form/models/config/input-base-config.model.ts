export enum transformationText {
  lowerCase,
  upperCase,
  trim
}

export interface InputBaseConfigModel {
  label: string;
  placeholder: string;
  valueDefault: any;
  transformation?: transformationText[];
}
