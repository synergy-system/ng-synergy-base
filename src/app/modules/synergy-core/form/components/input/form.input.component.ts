import { Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { InputBaseConfigModel, transformationText } from '@synergy/core/form/models';


@Component({
  templateUrl: 'form.input.component.html',
  selector: 'app-synergy-form-input'
})
export class FormInputComponent implements OnInit, OnChanges {

  @Input() public config: InputBaseConfigModel = {
    label: '',
    placeholder: '',
    valueDefault: '',
    transformation: []
  };
  @Output() public pipe$ = new EventEmitter<any>();

  public field = '';

  constructor() {
  }

  ngOnInit(): void {
    this.field = this.config.valueDefault;
  }

  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes.config.currentValue ) {
      this.field = changes.config.currentValue.valueDefault;
      this.pipe$.emit(this.field);
    }
  }

  @HostListener('input', ['$event']) onInputChange( $event ) {
    this.field = $event.target.value;
    if ( this.config.transformation ) {
      this.config.transformation.forEach(trans => {
        switch ( trans ) {
          case transformationText.lowerCase:
            this.field = this.field.toLowerCase();
            break;
          case transformationText.upperCase:
            this.field = this.field.toUpperCase();
            break;
          case transformationText.trim:
            this.field = this.field.trim();
            break;
        }
      });
    }
    this.pipe$.emit(this.field);
  }
}
