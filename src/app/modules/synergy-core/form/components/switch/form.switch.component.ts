import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { InputBaseConfigModel } from '@synergy/core/form';

@Component({
  templateUrl: 'form.switch.component.html',
  selector: 'app-synergy-form-switch'
})
export class FormSwitchComponent implements OnInit, OnChanges {

  @Input() public config: InputBaseConfigModel = {
    label: '',
    placeholder: '',
    valueDefault: ''
  };
  @Output() public pipe$ = new EventEmitter<any>();
  public field = true;

  constructor() {
  }

  ngOnInit(): void {
    this.field = Boolean(this.config.valueDefault);
  }

  ngOnChanges( changes: SimpleChanges ): void {
    if ( changes.config.currentValue ) {
      this.field = changes.config.currentValue.valueDefault;
      this.pipe$.emit(this.field);
    }
  }

  public OnChange($event) {
    console.log(`OnChange => ${JSON.stringify($event.target.checked)}`);
    this.pipe$.emit($event.target.checked);
  }
}
