import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-core-modal-confirm',
  templateUrl: './confirm-modal.component.html'
})
export class ConfirmModalComponent implements OnInit {

  public title: string;
  public content: string;
  public onClose: Subject<any>;

  constructor( private bs: BsModalRef ) {
    this.title = 'Confirmacion';
    this.content = 'Favor confirme';
  }

  ngOnInit() {
    this.onClose = new Subject();
  }

  public onConfirm(): void {
    this.onClose.next(true);
    this.bs.hide();
  }

  public onCancel(): void {
    this.onClose.next(false);
    this.bs.hide();
  }

}
