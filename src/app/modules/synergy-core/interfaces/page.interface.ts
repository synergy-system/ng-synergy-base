export interface IPage {
  numberPage: number;
  enable: boolean;
}
