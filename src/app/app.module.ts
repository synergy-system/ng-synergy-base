import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppAsideModule, AppBreadcrumbModule, AppFooterModule, AppHeaderModule, AppSidebarModule, } from '@coreui/angular';
import { PerfectScrollbarConfigInterface, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { BsDropdownModule, TabsModule } from 'ngx-bootstrap';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { DefaultLayoutComponent } from './containers/default-layout';
import { RegisterComponent } from './views/register/register.component';
import { LoginComponent } from './views/login/login.component';
import { P500Component } from './views/error/500.component';
import { P404Component } from './views/error/404.component';

import { SynergyCoreModule } from '@synergy/core/synergy-core.module';
import { SynergyUsersModule } from '@synergy/users/synergy-users.module';
import { SynergyRolesModule } from '@synergy/roles/synergy-roles.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

const APP_CONTAINERS = [
  DefaultLayoutComponent
];


const SYNERGY_MODULES = [
  SynergyCoreModule,
  SynergyUsersModule,
  SynergyRolesModule
];

const DefaultGlobalConfig = {
  progressBar: true,
  onActivateTick: true
};

@NgModule( {
  declarations: [
    AppComponent,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    ...APP_CONTAINERS,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ToastrModule.forRoot( DefaultGlobalConfig ),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    AppRoutingModule,
    ...SYNERGY_MODULES
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [AppComponent]
} )
export class AppModule {
}
